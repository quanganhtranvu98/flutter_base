FROM ubuntu:latest
FROM openjdk:11-jdk-slim

# Install dependencies
RUN apt-get update && apt-get install -y \
    unzip \
    xz-utils \
    curl \
    git \
    npm


# Set up Android SDK
ENV ANDROID_HOME /opt/android-sdk
ENV ANDROID_SDK_ROOT $ANDROID_HOME

RUN apt-get update && \
    apt-get install -y wget unzip && \
    wget -q https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip && \
    mkdir -p ${ANDROID_HOME}/cmdline-tools && \
    unzip -qq commandlinetools-linux-6609375_latest.zip -d ${ANDROID_HOME}/cmdline-tools && \
    rm commandlinetools-linux-6609375_latest.zip && \
    yes | ${ANDROID_HOME}/cmdline-tools/tools/bin/sdkmanager --licenses && \
    ${ANDROID_HOME}/cmdline-tools/tools/bin/sdkmanager "platform-tools" "platforms;android-30" "build-tools;30.0.3" && \
    rm -rf /var/lib/apt/lists/*

ENV PATH $PATH:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/cmdline-tools/latest/bin

# Set up Flutter
RUN git clone --branch stable https://github.com/flutter/flutter.git /opt/flutter
ENV PATH="$PATH:/opt/flutter/bin"

# List available Android SDKs
# RUN sdkmanager --list

# Check Flutter doctor
RUN flutter doctor

RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install -y nodejs

# Install Firebase CLI
RUN npm install -g firebase-tools
