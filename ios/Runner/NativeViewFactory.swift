//
//  NativeViewFactory.swift
//  Runner
//
//  Created by Quang Anh on 08/04/2023.
//

import Foundation
import UIKit
import Flutter
import SwiftUI


class NativeViewFactory : NSObject, FlutterPlatformViewFactory {
    
    public func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
        return NativeView(frame, viewId:viewId, args:args)
    }
}

public class NativeView : NSObject, FlutterPlatformView {
    
    let frame : CGRect
    let viewId : Int64
    
    init(_ frame:CGRect, viewId:Int64, args: Any?){
        self.frame = frame
        self.viewId = viewId
    }
    
    public func view() -> UIView {
        let myView : UIView = UIView(frame: self.frame)
        myView.backgroundColor = UIColor.lightGray
        let homeShopScreen = UIHostingController(rootView: HomeScreen())
        
        return MySwiftUIViewWrapper()
    }
    
}

class MySwiftUIViewWrapper: UIView {
    
    private let hostingController: UIHostingController<HomeScreen>
    
    init() {
        let swiftUIView = HomeScreen()
        self.hostingController = UIHostingController(rootView: swiftUIView)
        super.init(frame: .zero)
        self.addSubview(self.hostingController.view)
        self.hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        self.hostingController.view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.hostingController.view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.hostingController.view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.hostingController.view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
