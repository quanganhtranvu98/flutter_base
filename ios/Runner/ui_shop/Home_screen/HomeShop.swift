//
//  HomeScreen.swift
//  Base_swift
//
//  Created by Quang Anh on 02/04/2023.
//

import SwiftUI

struct HomeScreen: View {
    @State private var selectedIndex: Int = 0
    private let categories = ["All", "Chain", "Sofa", "Lamp", "Kitchen", "Table"]
    @ObservedObject var homeController = HomeController()
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("Bg").edgesIgnoringSafeArea(.all)
                ScrollView {
                    VStack (alignment: .leading) {
                        AppBarView(homeController: homeController)
                        Title().padding()
                        SeachAndScanView(homeController: homeController)
                        
                        
                        ScrollView(.horizontal,showsIndicators: false) {
                            HStack{
                                ForEach(0 ..< categories.count) { i in
                                    CategoryView(isActive: i == selectedIndex, text: categories[i])
                                        .onTapGesture {
                                            selectedIndex = i
                                        }
                                }
                            }
                        }
                        .padding()
                        
                        Text("Popular")
                            .font(.custom("PlayfairDisplay-Bold", size: 24))
                            .padding(.horizontal)
                        
                        ScrollView (.horizontal, showsIndicators: false) {
                            HStack {
                                ForEach(0 ..< 4) { index in
                                    ProductCardView(image: Image("chair_\(index+1)"), size: 210)
                                        .onTapGesture {
                                            homeController.showAlerDevelop()
                                        }
                                }
                                .padding(.trailing)
                            }
                            .padding(.horizontal)
                        }
                        
                        Text("Best")
                            .font(.custom("PlayfairDisplay-Bold", size: 24))
                            .padding(.horizontal)
                            .padding(.top)
                        
                        ScrollView (.horizontal, showsIndicators: false) {
                            HStack {
                                ForEach(0 ..< 4) { index in
                                    ProductCardView(image: Image("chair_\(index+1)"), size: 180)
                                    NavigationLink ( destination: DetailScreen(),label: {
                                        ProductCardView(image: Image("chair_\(index+1)"), size: 180)
                                    })
                                    .navigationBarHidden(true)
                                    .foregroundColor(.black)
                                    
                                }
                                .padding(.trailing)
                            }
                            .padding(.horizontal)
                        }
                    }
                }.overlay(
                    ToastView(isPresented: $homeController.showToast) {
                        Text("Developing.")
                    }
                )
                
                
                // bottom bar
                
                HStack{
                    BottomNavBarItem(image: Image("Home")) {}
                    BottomNavBarItem(image: Image("Home")) {}
                    BottomNavBarItem(image: Image("Home")) {}
                    BottomNavBarItem(image: Image("User")) {}
                }
                .padding()
                .background(Color.white)
                .clipShape(Capsule())
                .padding()
                .shadow(color: Color.black.opacity(0.15), radius: 8,x: 2,y: 6)
                .frame(maxHeight: .infinity, alignment: .bottom)
            }
        }
    }
}

struct HomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreen()
    }
}

struct AppBarView: View {
    @ObservedObject var homeController: HomeController
    
    var body: some View {
        VStack{
            HStack{
                Button(action: {}) {
                    Image("menu").padding().background(Color(.white)).cornerRadius(10)
                }
                
                Spacer()
                
                Image(uiImage: homeController.avatarImage)
                    .resizable()
                    .frame(width: 42, height: 42)
                    .cornerRadius(10)
                    .onTapGesture {
                        homeController.setShowPickerAvatar()
                    }

            }.padding()
        } .sheet(isPresented: $homeController.isShowPhotoPicker, content: {
            PhotoPicker(avatarImage: $homeController.avatarImage)
        })
    }
}

struct Title: View {
    var body: some View {
        Text("Find The \nBest").font(.custom("PlayfairDisplay-Regular", size: 28))
            .foregroundColor(Color("Primary"))
        + Text("Furniture!").font(.custom("PlayfairDisplay-Bold", size: 28))
            .foregroundColor(Color("Primary"))
    }
}

struct SeachAndScanView: View {
    @State private var search: String = ""
    let homeController: HomeController
    
    var body: some View {
        HStack{
            HStack {
                Image("Search").padding(.trailing,8)
                TextField(
                    "Search item...", text: $search
                )
                
            }.padding(.all, 20)
                .background(Color.white)
                .cornerRadius(10)
                .padding(.horizontal)
            
            Button {
                homeController.showAlerDevelop()
            } label: {
                Text("Scan")
                    .foregroundColor(.white)
                    .padding()
                    .background(Color("Primary"))
                    .cornerRadius(10)
            }
        }
        .padding(.trailing,10)
    }
}

struct CategoryView: View {
    let isActive: Bool
    let text: String
    
    var body: some View {
        VStack (alignment: .center, spacing: 0){
            Text(text)
                .font(.system(size: 17)).fontWeight(.medium)
                .foregroundColor(isActive ? Color("Primary") : Color.black.opacity(0.6))
            
            
            if(isActive){
                Color("Primary")
                    .frame(width: 15, height: 2)
                    .clipShape(Capsule())
            }
        }
        .padding(.trailing)
    }
}

struct ProductCardView: View {
    let image : Image
    let size: CGFloat
    var body: some View {
        VStack{
            image
                .resizable()
                .frame(width: size, height: 200 * (size/210), alignment: .center)
                .cornerRadius(20)
            
            Text("Luxury Swedian chair")
                .font(.title3)
                .fontWeight(.bold)
            
            HStack (spacing: 2){
                ForEach(/*@START_MENU_TOKEN@*/0 ..< 5/*@END_MENU_TOKEN@*/) { item in
                    Image("star")
                }
                
                Spacer()
                
                Text("$1000")
                    .font(.title3)
                    .fontWeight(.bold)
            }
        }
        .frame(width: 210)
        .padding()
        .background(Color.white)
        .cornerRadius(20)
    }
}

struct BottomNavBarItem: View {
    let image : Image
    let action : ()-> Void
    
    var body: some View {
        Button(action: action, label: {
            image
                .frame(maxWidth: .infinity)
        })
    }
}
