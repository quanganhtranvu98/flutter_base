//
//  Home_controller.swift
//  Runner
//
//  Created by Quang Anh on 16/04/2023.
//“

import SwiftUI

class HomeController: ObservableObject {
    @Published var showToast = false
    
    @Published var isShowPhotoPicker = false;
    @Published var avatarImage = UIImage(imageLiteralResourceName: "Profile");
    
    func showAlerDevelop() {
        showToast = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.showToast = false
        }
    }
    
    func setShowPickerAvatar(){
        isShowPhotoPicker = !isShowPhotoPicker;
    }
}
