//
//  Photo_picker.swift
//  Runner
//
//  Created by Quang Anh on 23/04/2023.
//

import SwiftUI

struct PhotoPicker: UIViewControllerRepresentable {
    @Binding var avatarImage: UIImage
    
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.allowsEditing = true
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(photo: self)
    }

    final class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate{

        let photo: PhotoPicker

        init(photo: PhotoPicker) {
            self.photo = photo
        }

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[.editedImage] as? UIImage{
                guard let data = image.jpegData(compressionQuality: 0.1), let compressedImage = UIImage(data: data) else {
                    //show return error
                    
                    return ;
                }
                photo.avatarImage = compressedImage
            } else{
                // return error show an alert

            }
            
            picker.dismiss(animated: true)
        }

    }
    
}

//struct Photo_picker_Previews: PreviewProvider {
//    static var previews: some View {
//        PhotoPicker()
//    }
//}
