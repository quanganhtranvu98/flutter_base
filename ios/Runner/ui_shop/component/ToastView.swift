//
//  ToastView.swift
//  Runner
//
//  Created by Quang Anh on 16/04/2023.
//

import SwiftUI

struct ToastView<Content: View>: View {
    @Binding var isPresented: Bool
    let content: () -> Content
    
    var body: some View {
        ZStack(alignment: .center) {
            if isPresented {
                VStack {
                    content().padding()
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .background(Color.gray)
                .foregroundColor(.white)
                .cornerRadius(10)
                .padding(EdgeInsets(top: 30, leading: 30, bottom: 30, trailing: 30))
                .transition(.opacity)
                .animation(.easeInOut(duration: 0.3))
            }
        }
    }
}
