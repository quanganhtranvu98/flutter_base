import 'package:game_center/presentation/show_shop_native/ui/show_shop_native_screen.dart';
import 'package:game_center/router/general_router.dart';
import 'package:game_center/router/router_define.dart';

class ShowShopPageRoute extends RouteDefine {
  @override
  Path initRoute(dynamic arguments) {
    return Path(
      builder: (_) => const ShowShopNativeScreen(),
    );
  }

  static Future<dynamic> pushNamed() {
    return GeneralRouter.push<ShowShopPageRoute>();
  }
}