import 'package:flutter/material.dart';
import 'dart:io' show Platform;

class ShowShopNativeScreen extends StatelessWidget {
  const ShowShopNativeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Platform.isIOS
            ? const UiKitView(
                viewType: 'NativeView',
              )
            : const SizedBox(),
      ),
    );
  }
}
